﻿using SinemaRezervasyon.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SinemaRezervasyon.Controllers
{
    public class HomeController : Controller
    {
         
       
        private SinemaRezervasyonEntities db = new SinemaRezervasyonEntities();
        public ActionResult Index()
        {
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.slider = db.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
            obj.haberler = db.Haberler.OrderByDescending(x => x.HaberID).Take(3).ToList();
            obj.filmler = db.Film.OrderByDescending(x => x.VizyonTarihi).Take(6).ToList();
            obj.reklam = db.Reklam.Take(3).ToList();
            return View("Index", obj);
            
        }
        public ActionResult Contact()
        {

            return View();
        }


        [HttpPost]
        public ActionResult Contact(Iletisim iletisimform)
        {
            try
            {
                Iletisim _iletisimform = new Iletisim();
                _iletisimform.AdSoyad = iletisimform.AdSoyad;
                _iletisimform.ePosta = iletisimform.ePosta;
                _iletisimform.Mesaj = iletisimform.Mesaj;
                _iletisimform.Tarih = DateTime.Now;
                db.Iletisim.Add(_iletisimform);
                db.SaveChanges();
                TempData["Mesaj"] = "Form Başarıyla gönderilmiştir.";
                return View();
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult Test()
        {
            return View();
        }


        public ActionResult MesajGonder(string adi,string email,string msg)
        {
            try
            {
                Iletisim _iletisimform = new Iletisim();
                _iletisimform.AdSoyad = adi;
                _iletisimform.ePosta = email;
                _iletisimform.Mesaj = msg;
                _iletisimform.Tarih = DateTime.Now;
                db.Iletisim.Add(_iletisimform);
                db.SaveChanges();
                TempData["Mesaj"] = "Form Başarıyla gönderilmiştir.";
                return View("Contact");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        [ValidateInput(false)]
        public ActionResult BiletControl(SalonKoltuk salonkoltuk ,string biletfiyat,string biletstring,string filmadi )
        {
            ViewBag.filmadi = filmadi;

            ViewBag.biletfiyati = biletfiyat;

            List<string> liste = new List<string>();
            string biletstr = biletstring.Trim();
            
            string[] biletlist = Regex.Split(biletstr, "choosen-place");
            int i = 0;
            foreach (var bilet in biletlist)
            {
                if(i != 0)
                {
                    string parca = bilet.Substring(0, 5).Trim();
                    liste.Add(parca);
                }
                i++;
            }
            ArrayList listarr = new ArrayList();
            foreach (var aytim in liste)
            {
                listarr.Add(aytim);
            }
            foreach (var item in liste)
            {
                var salonkoltugu = db.SalonKoltuk.Where(x => x.Koltuk.KoltukAd == item).First();
                salonkoltugu.DoluMu = true;
                db.SaveChanges();
            }
            listarr.Reverse();
            ViewBag.Liste = listarr;
            return View("BiletFinal",liste);

        }

        public ActionResult Bilet(int SalonId,string FilmAdi)
        {
            ViewBag.filmAdi = FilmAdi;
            List<SalonKoltuk> koltuklist = db.SalonKoltuk.Where(x => x.SalonId == SalonId).ToList();
            return View(koltuklist);
        }
        public ActionResult BiletFinal()
        {
            return View();
        }

        public ActionResult FilmBilgisi(string filmadi)
        {
            ViewBag.filmadi = filmadi;
            return View(db.Film.Where(o => o.FilmAdi == filmadi).First());
        }


        
        public ActionResult News()
        {
            return View(db.Haberler.ToList());
        }
        public ActionResult ResimEkle()
        {
            
            return View();
        }
        public ActionResult Login()
        {
            

            return View();
        }


        public ActionResult Vision()
        {
            return View(db.Film.ToList());
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult RegisterMessage()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public static string KullaniciBilgiGetir(string eposta)
        {
            var kul = new SinemaRezervasyonEntities();
            var kul_bilgi = kul.Uye.FirstOrDefault(x => x.ePosta == eposta);
            string adsoyad = kul_bilgi.Adi;
            return adsoyad;
        }
        public ActionResult UyeEkle(RegisterViewModel RegisterModel)
        {
            var db = new SinemaRezervasyonEntities();
            Uye _uye = new Uye();
            string aktivasyonkodu = Guid.NewGuid().ToString("N").Substring(0, 20).ToUpper();
            _uye.Adi = RegisterModel.Adi;
            _uye.Soyadi = RegisterModel.Soyadi;
            _uye.Telefon = RegisterModel.Telefon;
            _uye.ePosta = RegisterModel.ePosta;
            _uye.Sifre = FormsAuthentication.HashPasswordForStoringInConfigFile(RegisterModel.Sifre, "md5");
            _uye.Aktivasyon = aktivasyonkodu;
            _uye.Onay = false;
            _uye.Yetki = "Kullanici";
            _uye.KayitTarihi = DateTime.Now;
            db.Uye.Add(_uye);
            db.SaveChanges();
            string mailIcerik = "STCINEMAS AİLESİNE HOŞGELDİNİZ<br>www.stcinemas.com a kayıt olduğunuz için teşekkür ederiz. <br> Üyeliğinizi onaylamak için doğrulamak için aşağıdaki linke tıklayınız.<br> ";
            mailIcerik += "<a href=\"http://stcinemas.com.tr/Home/Aktivasyon?Eposta=" + RegisterModel.ePosta + "&AktivasyonKodu=" + aktivasyonkodu + "\" style=\"text-decoration:underline; color:#3ba5d8;\">Üyeliğimi Aktifleştir</a>";
            //MailGonder(RegisterModel.ePosta, "STCINEMAS ~ Üyelik Aktivasyon", mailIcerik);
            return View("RegisterMessage");
        }
        //public ActionResult UyeGiris(RegisterViewModel LoginModel)
        //{ }
        public ActionResult UyeGiris(RegisterViewModel LoginModel)
        {
           
                var db = new SinemaRezervasyonEntities();
                string sifre = FormsAuthentication.HashPasswordForStoringInConfigFile(LoginModel.Sifre, "md5");
                var uye = db.Uye.Where(x => x.ePosta == LoginModel.ePosta && x.Sifre == sifre && x.Onay == true).FirstOrDefault();
                if (uye != null)
                {
                    Session["eposta"] = LoginModel.ePosta;
                    Session["role"] = uye.Yetki;
                    if (uye.Yetki != "Admin")
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Slider","Admin");
                    }
                }
                else
                {
                    TempData["Mesaj"] = "Kullanıcı Adı veya şifre yanlış, eposta aktivasyon maili onaylanmamıştır";
                }
            return View("Login");
        }


        //public static void MailGonder(string kime, string baslik, string mesajIcerik)
        //{
        //    string server = "smtp.gmail.com";
        //    string kullaniciAdi = "uyelik@stcinemas.com.tr";
        //    string sifre = "xxxxxxxxxx";
        //    int port = 587;
        //    SmtpClient smtpclient = new SmtpClient();
        //    smtpclient.Port = port; //Smtp Portu (Sunucuya Göre Değişir)
        //    smtpclient.Host = server; //Smtp Hostu (Gmail smtp adresi bu şekilde)
        //    smtpclient.EnableSsl = true; //Sunucunun SSL kullanıp kullanmadıgı
        //    smtpclient.Credentials = new NetworkCredential(kullaniciAdi, sifre); //Gmail Adresiniz ve Şifreniz
        //    MailMessage mail = new MailMessage();
        //    mail.From = new MailAddress(kullaniciAdi, "STCINEMAS"); //Gidecek Mail Adresi ve Görünüm Adınız
        //    mail.To.Add(kime); //Kime Göndereceğiniz
        //    mail.Subject = baslik; //Emailin Konusu
        //    mail.Body = mesajIcerik; //Mesaj İçeriği
        //    mail.IsBodyHtml = true; //Mesajınızın Gövdesinde HTML destegininin olup olmadıgı
        //    smtpclient.Send(mail);
        //}


        public ActionResult Aktivasyon(string eposta, string aktivasyonkodu)
        {
            var db = new SinemaRezervasyonEntities();
            var uye = db.Uye.Where(x => x.ePosta == eposta && x.Aktivasyon == aktivasyonkodu).FirstOrDefault();
            if (uye != null)
            {
                uye.Onay = true;
                db.SaveChanges();
                return View();
            }
            return RedirectToAction("Index", "Home");
        }


        public ActionResult Cikis()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        public static string RoleGetir(string eposta)
        {
            var db = new SinemaRezervasyonEntities();
            var uye_rol = db.Uye.FirstOrDefault(x => x.ePosta == eposta).Yetki;
            return uye_rol;
        }


        
    }


    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<Haberler> haberler { get; set; }
        public List<Film> filmler { get; set; }
        public List<Tur> tur { get; set; }
        public List<YasSinir> yassinir { get; set; }
        public List<Uye> uye { get; set; }
        public List<Yorum> yorum { get; set; }
        public List<Iletisim> iletisim { get; set; }
        public List<Salonlar> salon { get; set; }
        public List<Reklam> reklam { get; set; }

    }

}