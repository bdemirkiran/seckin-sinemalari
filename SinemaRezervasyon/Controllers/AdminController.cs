﻿using SinemaRezervasyon.Models;
using SinemaRezervasyon.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EgtMerkez.Controllers
{
    public class AdminController : Controller
    {
        SinemaRezervasyonEntities db = new SinemaRezervasyonEntities();

        #region // Slider
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Slider()
        {
            var slider = db.Slider.ToList();
            return View(slider);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult SlideEkle()
        {
            return View();
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult SlideDuzenle(int SlideID)
        {
            var _slideDuzenle = db.Slider.Where(x => x.ID == SlideID).FirstOrDefault();
            return View(_slideDuzenle);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult SlideSil(int SlideID)
        {
            try
            {
                db.Slider.Remove(db.Slider.First(d => d.ID == SlideID));
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult SlideEkle(Slider s, HttpPostedFileBase file)
        {
            try
            {
                Slider _slide = new Slider();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.SliderFoto = memoryStream.ToArray();
                }
                _slide.SliderText = s.SliderText;
                _slide.BaslangicTarih = s.BaslangicTarih;
                _slide.BitisTarih = s.BitisTarih;
                db.Slider.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult SlideDuzenle(Slider slide, HttpPostedFileBase file)
        {
            try
            {
                var _slideDuzenle = db.Slider.Where(x => x.ID == slide.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slideDuzenle.SliderFoto = memoryStream.ToArray();
                }
                _slideDuzenle.SliderText = slide.SliderText;
                _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                _slideDuzenle.BitisTarih = slide.BitisTarih;
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

        #region Film
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Film()
        {
            AnaSayfaDTO nesne = new AnaSayfaDTO();
            nesne.filmler = db.Film.OrderByDescending(x => x.VizyonTarihi).ToList();
            nesne.tur = db.Tur.OrderBy(x => x.TurAd).ToList();
            nesne.yassinir = db.YasSinir.OrderBy(x => x.YasSinirAd).ToList();
            return View("Film", nesne);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult FilmSil(int FilmID)
        {
            try
            {
                db.Film.Remove(db.Film.First(d => d.FilmID == FilmID));
                db.SaveChanges();
                return RedirectToAction("Film", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult FilmEkle()
        {
            ViewBag.Salonlar = new SelectList(db.Salonlar.OrderBy(x => x.Adi).ToList(), "SalonId", "Adi");
            ViewBag.Tur = new SelectList(db.Tur.OrderBy(x => x.TurAd).ToList(), "TurID", "TurAd");
            ViewBag.YasSinir = new SelectList(db.YasSinir.OrderBy(x => x.YasSinirAd).ToList(), "YasSinirID", "YasSinirAd");
            return View();
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult FilmDuzenle(int FilmID)
        {
            ViewBag.Salonlar = new SelectList(db.Salonlar.OrderBy(x => x.Adi).ToList(), "SalonId", "Adi");
            ViewBag.Tur = new SelectList(db.Tur.OrderBy(x => x.TurAd).ToList(), "TurID", "TurAd");
            ViewBag.YasSinir = new SelectList(db.YasSinir.OrderBy(x => x.YasSinirAd).ToList(), "YasSinirID", "YasSinirAd");
            var _filmDuzenle = db.Film.Where(x => x.FilmID == FilmID).FirstOrDefault();
            return View(_filmDuzenle);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult FilmEkle(Film film, HttpPostedFileBase file)
        {
            try
            {
                Film _film = new Film();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _film.Fotografi = memoryStream.ToArray();
                }
                _film.FilmAdi = film.FilmAdi;
                _film.Konu = film.Konu;
                _film.Yonetmen = film.Yonetmen;
                _film.OyuncuAd = film.OyuncuAd;
                _film.TurID = film.TurID;
                _film.Sure = film.Sure;
                _film.YasSinirID = film.YasSinirID;
                _film.Ulke = film.Ulke;
                _film.VizyonTarihi = film.VizyonTarihi;
                _film.SalonId = film.SalonId;
                db.Film.Add(_film);
                db.SaveChanges();
                return RedirectToAction("Film", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult FilmDuzenle(Film film, HttpPostedFileBase file)
        {
            try
            {
                var _filmDuzenle = db.Film.Where(x => x.FilmID == film.FilmID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _filmDuzenle.Fotografi = memoryStream.ToArray();
                }
                _filmDuzenle.FilmAdi = film.FilmAdi;
                _filmDuzenle.Konu = film.Konu;
                _filmDuzenle.Yonetmen = film.Yonetmen;
                _filmDuzenle.VizyonTarihi = film.VizyonTarihi;
                _filmDuzenle.OyuncuAd = film.OyuncuAd;
                _filmDuzenle.TurID = film.TurID;
                _filmDuzenle.Sure = _filmDuzenle.Sure;
                _filmDuzenle.YasSinirID = _filmDuzenle.YasSinirID;
                _filmDuzenle.Ulke = _filmDuzenle.Ulke;
                _filmDuzenle.SalonId = film.SalonId;
                db.SaveChanges();
                return RedirectToAction("Film", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }

        #endregion



        #region // Haberler
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Haberler()
        {
            var Haberler = db.Haberler.ToList();
            return View(Haberler);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult HaberEkle()
        {
            return View();
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult HaberSil(int HaberID)
        {
            try
            {
                db.Haberler.Remove(db.Haberler.First(d => d.HaberID == HaberID));
                db.SaveChanges();
                return RedirectToAction("Haberler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult HaberDuzenle(int HaberID)
        {
            var _haberDuzenle = db.Haberler.Where(x => x.HaberID == HaberID).FirstOrDefault();
            return View(_haberDuzenle);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult HaberEkle(Haberler h, HttpPostedFileBase file)
        {

            try
            {
                Haberler _haber = new Haberler();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _haber.Fotografi = memoryStream.ToArray();
                }
                _haber.Yazi = h.Yazi;
                _haber.Tarih = h.Tarih;
                _haber.Baslik = h.Baslik;
                db.Haberler.Add(_haber);
                db.SaveChanges();
                return RedirectToAction("Haberler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult HaberDuzenle(Haberler haber, HttpPostedFileBase file)
        {
            try
            {
                var _haberDuzenle = db.Haberler.Where(x => x.HaberID == haber.HaberID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _haberDuzenle.Fotografi = memoryStream.ToArray();
                }
                _haberDuzenle.Yazi = haber.Yazi;
                _haberDuzenle.Tarih = haber.Tarih;
                _haberDuzenle.Baslik = haber.Baslik;
                db.SaveChanges();
                return RedirectToAction("Haberler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion


        #region // Reklam
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Reklam()
        {
            var reklam = db.Reklam.ToList();
            return View(reklam);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult ReklamEkle()
        {
            return View();
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult ReklamDuzenle(int ReklamID)
        {
            var _reklamDuzenle = db.Reklam.Where(x => x.ReklamID == ReklamID).FirstOrDefault();
            return View(_reklamDuzenle);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult ReklamSil(int ReklamID)
        {
            try
            {
                db.Reklam.Remove(db.Reklam.First(d => d.ReklamID == ReklamID));
                db.SaveChanges();
                return RedirectToAction("Reklam", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult ReklamEkle(Reklam r, HttpPostedFileBase file)
        {
            try
            {
                Reklam _reklam = new Reklam();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _reklam.Fotograf = memoryStream.ToArray();
                }

                db.Reklam.Add(_reklam);
                db.SaveChanges();
                return RedirectToAction("Reklam", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult ReklamDuzenle(Reklam reklam, HttpPostedFileBase file)
        {
            try
            {
                var _reklamDuzenle = db.Reklam.Where(x => x.ReklamID == reklam.ReklamID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _reklamDuzenle.Fotograf = memoryStream.ToArray();
                }
                db.SaveChanges();
                return RedirectToAction("Reklam", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

        #region // Salon
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Salon()
        {
            AnaSayfaDTO nesne = new AnaSayfaDTO();
            nesne.salon = db.Salonlar.OrderBy(x => x.SalonId).ToList();
            nesne.filmler = db.Film.ToList();
            return View("Salon", nesne);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult SalonEkle()
        {
            return View();
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult SalonDuzenle(int SalonID)
        {
            var _salonDuzenle = db.Salonlar.Where(x => x.SalonId == SalonID).FirstOrDefault();
            return View(_salonDuzenle);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]

        [HttpPost]
        public ActionResult SalonEkle(Salonlar s)
        {
            try
            {
                Salonlar _salon = new Salonlar();
                _salon.Adi = s.Adi;
                db.Salonlar.Add(_salon);
                db.SaveChanges();
                return RedirectToAction("Salon", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        [HttpPost]
        public ActionResult SalonDuzenle(Salonlar salon, HttpPostedFileBase file)
        {
            try
            {
                var _salonDuzenle = db.Salonlar.Where(x => x.SalonId == salon.SalonId).FirstOrDefault();
                _salonDuzenle.Adi = salon.Adi;
                db.SaveChanges();
                return RedirectToAction("Salon", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }
        #endregion

        #region // Iletisim
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult Iletisim()
        {
            AnaSayfaDTO nesne = new AnaSayfaDTO();
            nesne.iletisim = db.Iletisim.OrderBy(x => x.IletisimID).ToList();
            return View("Iletisim", nesne);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]

        public ActionResult IletisimSil(int IletisimID)
        {
            try
            {
                db.Iletisim.Remove(db.Iletisim.First(d => d.IletisimID == IletisimID));
                db.SaveChanges();
                return RedirectToAction("Iletisim", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

        #endregion

        #region // Uye
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult KullaniciOnay()
        {
            AnaSayfaDTO nesne = new AnaSayfaDTO();
            nesne.uye = db.Uye.Where(x => x.Onay == false).ToList();
            return View("KullaniciOnay",nesne);
        }
        [SinemaRezervasyon.Attributes.AdminRoleControl]
        public ActionResult KullaniciOnayla(Uye uye, HttpPostedFileBase file)
        {
            try
            {
                var _uyeDuzenle = db.Uye.Where(x => x.KullaniciID == uye.KullaniciID).FirstOrDefault();
               
                _uyeDuzenle.Onay = true;
                db.SaveChanges();
                return RedirectToAction("KullaniciOnay", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }
        #endregion

    }

}