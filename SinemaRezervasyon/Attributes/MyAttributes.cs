﻿using SinemaRezervasyon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SinemaRezervasyon.Attributes
{
    public class AdminRoleControl : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // action çalışmadan önce yapılacak işlemler

            var db = new SinemaRezervasyonEntities();
            if (HttpContext.Current.Session["eposta"] == null)
            {
                filterContext.HttpContext.Response.Redirect("~/Home/Login");
            }
            else
            {
                string ePosta = HttpContext.Current.Session["eposta"].ToString();
                var uye_rol = db.Uye.FirstOrDefault(x => x.ePosta == ePosta).Yetki;
                if (uye_rol != "Admin")
                {
                    filterContext.HttpContext.Response.Redirect("~/Home/Index");
                }
            }
            base.OnActionExecuting(filterContext);
        }

    }
}