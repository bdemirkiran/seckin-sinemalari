﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SinemaRezervasyon.Startup))]
namespace SinemaRezervasyon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
