﻿using System.Web;
using System.Web.Optimization;

namespace SinemaRezervasyon
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
           
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/external/idangerous.swiper.css",
                      "~/Content/css/external/jquery.selectbox.css",
                      "~/Content/css/external/magnific-popup.css",
                      "~/Content/rs-plugin/css/settings.css",
                      "~/Content/css/style.css",
                      "~/Content/css/touch.css"));

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                   "~/Content/js/external/count.down.js",
                   "~/Content/js/external/form-element.js",
                   "~/Content/js/external/idangerous.swiper.min.js",
                   "~/Content/js/external/infobox.js",
                   "~/Content/js/external/jquery-1.10.1.min.js",
                   "~/Content/js/external/jquery-migrate-1.2.1.min.js",
                   "~/Content/js/external/jquery.inview.js",
                   "~/Content/js/external/jquery.knob.js",
                   "~/Content/js/external/jquery.magnific-popup.min.js",
                   "~/Content/js/external/jquery.raty.js",
                   "~/Content/js/external/jquery.selectbox-0.2.min.js",
                   "~/Content/js/external/modernizr.custom.js",
                   "~/Content/js/external/twitterfeed.js",
                   "~/Content/js/external/min/count.down.min.js",
                   "~/Content/js/external/min/jquery.knob.min.js",
                   "~/Content/js/external/min/twitterfeed.min.js",


                    "~/Content/js/custom.js",
                    "~/Content/js/form.js",
                    "~/Scripts/jquery.mobile.menu.js",

                     "~/Content/js/min/custom.min.js",
                    "~/Content/js/min/form.min.js",
                    "~/Scripts/min/jquery.mobile.menu.min.js"
                    ));

            bundles.Add(new StyleBundle("~/Content/rs-plugin/css").Include(
                     "~/Content/rs-plugin/css/settings-ie8.cssd",
                     "~/Content/rs-plugin/css/settings.css"));

            bundles.Add(new ScriptBundle("~/Content/rs-plugin/js").Include(
                   "~/Content/rs-plugin/js/jquery.themepunch.plugins.min.js",
                    "~/Content/rs-plugin/js/jquery.themepunch.revolution.js",
                    "~/Content/rs-plugin/js/jquery.themepunch.revolution.min.js"));


            //* Admin *//

            bundles.Add(new StyleBundle("~/assets/css/styles.css").Include(
                      "~/assets/css/styles.css",
                      "~/assets/demo/variations/header-blue.css"));

            bundles.Add(new StyleBundle("~/assets/css/ie8.css").Include(
                      "~/assets/css/ie8.css"));

            bundles.Add(new ScriptBundle("~/assets/js/plugin-before").Include(
                      "~/assets/js/jqueryui-1.10.3.min.js",
                      "~/assets/js/bootstrap.min.js",
                      "~/assets/js/enquire.js",
                      "~/assets/js/jquery.cookie.js",
                      "~/assets/js/jquery.nicescroll.min.js"));

            bundles.Add(new ScriptBundle("~/assets/js/plugin-after").Include(
                      "~/assets/js/placeholdr.js",
                      "~/assets/js/application.js"));

            bundles.Add(new ScriptBundle("~/Template/js/master_core.js").Include(
                      "~/assets/js/master_core.js"));

            bundles.Add(new ScriptBundle("~/Template/js/showLoading.js").Include(
                     "~/assets/js/showLoading.js"));

            bundles.Add(new StyleBundle("~/Template/css/master_styles.css").Include(
                      "~/assets/css/master_styles.css"));






















        }
    }
}
