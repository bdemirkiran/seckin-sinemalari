﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SinemaRezervasyon.Models
{
    [MetadataType(typeof(FilmMetadata))]
    public partial class Film
    {
    }

    [MetadataType(typeof(HaberlerMetadata))]
    public partial class Haberler
    {
    }

}